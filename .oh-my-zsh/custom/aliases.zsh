alias mntdev='sudo mount -t cifs //se2.intranet.ontimenet.com/development ~/mnt/development/ -o user="thoso",uid=1000,gid=1000'
alias mntusers='sudo mount -t cifs //se2.intranet.ontimenet.com/users ~/mnt/users/ -o user="thoso",uid=1000,gid=1000'
alias mntwsevst='sudo mount -t cifs //WSEVST-S0001.westermo.com/WSEVST ~/mnt/wsevst/ -o user="thoso",uid=1000,gid=1000'
alias mntwsevstdev='sudo mount -t cifs //WSEVST-S0001.westermo.com/WSEVST/development ~/mnt/development/ -o user="thoso",uid=1000,gid=1000'
alias mntwsevstusers='sudo mount -t cifs //WSEVST-S0001.westermo.com/WSEVST/users ~/mnt/users/ -o user="thoso",uid=1000,gid=1000'

alias mntwemo='sudo mount -t cifs //wijsesrv010.wij.se/wemo/ ~/mnt/wemo/ -o user="tse",uid=1000,gid=1000'

alias gnome-control-center-myalias='env XDG_CURRENT_DESKTOP=GNOME gnome-control-center'
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
#alias vpnwork='cd $HOME/Documents/wmo/THOSO@remote.rd.westermo.se; sudo openvpn THOSO@remote.rd.westermo.se.ovpn'
alias penv='sudo direnv exec . /usr/bin/python2.7'
alias k='kubectl'

# env_keep does not work on Ubuntu 18.04 so this is a workaround.
alias sudo='sudo -E env "PATH=$PATH PYTHONPATH=$PYTHONPATH"'
